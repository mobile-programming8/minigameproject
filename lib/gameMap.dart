import 'dart:math';

import 'monster.dart';
import 'player.dart';

class GameMap {
  static Map getMainGameMap() {
    List<Location> locationList = [];
    //Beginner Town
    List<String> beginnerTownConnection = [];
    beginnerTownConnection.add("Forest [Lv.1-5]");
    beginnerTownConnection.add("Mountain [Lv.6-10]");
    Location beginnerTown =
        Location(0, "Beginner Town", beginnerTownConnection, 0, true, []);
    locationList.add(beginnerTown);

    //Forest
    List<String> forestConnection = [];
    forestConnection.add("Beginner Town");

    List<Item> forestJunkItemList = [];
    forestJunkItemList.add(Item("Pendant [Key item]", false, 1, 1000, 0.0500));
    forestJunkItemList
        .add(Weapon("Wooden Sword [Rare]", "Sword", 5, 125, 1.1200));
    forestJunkItemList.add(Weapon("Wooden Bow [Rare]", "Bow", 10, 125, 1.1200));
    forestJunkItemList
        .add(Weapon("Wooden Staff [Rare]", "Staff", 10, 125, 1.1200));
    forestJunkItemList
        .add(Weapon("Wooden Mace [Rare]", "Mace", 7, 125, 1.1200));
    forestJunkItemList.add(JunkItem("Herb [Uncommon]", true, 1, 35, 15.25));
    forestJunkItemList
        .add(JunkItem("Slime gel [Common]", true, 1, 15, 20.5000));
    forestJunkItemList.add(JunkItem("Herb [Common]", true, 1, 5, 35.0000));
    forestJunkItemList.add(JunkItem("Stick [Common]", true, 1, 1, 100.0000));

    Location forest = Location(
        1, "Forest [Lv.1-5]", forestConnection, 0, false, forestJunkItemList);
    locationList.add(forest);

    //Mountain
    List<String> mountainConnection = [];
    mountainConnection.add("Beginner Town");
    mountainConnection.add("Mid Town");

    List<Item> mountainJunkItemList = [];
    mountainJunkItemList
        .add(Item("Mysterious Key [Key item]", false, 1, 1000, 0.0500));
    mountainJunkItemList
        .add(Weapon("Goblin knife [Epic]", "Sword", 35, 625, 0.5200));
    mountainJunkItemList
        .add(Weapon("Goblin Bow [Epic]", "Bow", 40, 625, 0.5200));
    mountainJunkItemList.add(JunkItem("Herb [Rare]", true, 1, 100, 15.25));
    mountainJunkItemList
        .add(JunkItem("Goblin cloth [Common]", true, 1, 45, 20.5000));
    mountainJunkItemList
        .add(JunkItem("Crystal [Common]", true, 1, 25, 35.0000));
    mountainJunkItemList
        .add(JunkItem("Hard rock [Common]", true, 1, 5, 100.00));

    Location mountain = Location(2, "Mountain [Lv.6-10]", mountainConnection, 6,
        false, mountainJunkItemList);
    locationList.add(mountain);

    //Mid Town
    List<String> midTownConnection = [];
    midTownConnection.add("Mountain [Lv.6-10]");
    midTownConnection.add("Cave [Lv.9-15]");
    midTownConnection.add("Deep Forset [Lv.13-24]");
    Location midTown = Location(3, "Mid Town", midTownConnection, 7, true, []);
    locationList.add(midTown);

    //Cave
    List<String> caveConnection = [];
    caveConnection.add("Mid Town");

    List<Item> caveJunkItemList = [];
    caveJunkItemList.add(Item("Lost key [Key item]", false, 1, 1000, 0.0050));
    caveJunkItemList
        .add(Weapon("Staff of Nemos [Epic]", "Staff", 55, 1625, 0.5200));
    caveJunkItemList
        .add(Weapon("High priest staff [Epic]", "Mace", 70, 1625, 0.5200));
    caveJunkItemList.add(Item("Magic crystal [Rare]", true, 1, 250, 6.25));
    caveJunkItemList
        .add(JunkItem("Dark bat's wing [Uncommon]", true, 1, 100, 12.5000));
    caveJunkItemList.add(JunkItem("Crystal [Uncommon]", true, 1, 45, 25.0000));
    caveJunkItemList.add(JunkItem("Ore [Uncommon]", true, 1, 15, 100.00));

    Location cave = Location(
        4, "Cave [Lv.9-15]", caveConnection, 9, false, caveJunkItemList);
    locationList.add(cave);

    //Deep Forest
    List<String> deepForestConnection = [];
    deepForestConnection.add("Mid Town");
    deepForestConnection.add("Tower [Lv.25+]");
    deepForestConnection.add("Capital");

    List<Item> deepForestJunkItemList = [];
    deepForestJunkItemList
        .add(Weapon("Bloodlust [Legendary]", "Sword", 205, 11725, 0.0100));
    deepForestJunkItemList
        .add(Weapon("Darkmoon Longbow [Legendary]", "Bow", 270, 11725, 0.0100));
    deepForestJunkItemList.add(Weapon(
        "Sage's Crystal Staff [Legendary]", "Staff", 255, 11725, 0.0100));
    deepForestJunkItemList
        .add(Weapon("Yorshka's Mace [Legendary]", "Mace", 170, 11725, 0.0100));
    deepForestJunkItemList
        .add(Item("Magic crystal [Epic]", true, 1, 500, 6.2500));
    deepForestJunkItemList
        .add(Item("Magic crystal [Rare]", true, 1, 250, 12.5000));
    deepForestJunkItemList
        .add(JunkItem("Dark Crystal [Uncommon]", true, 1, 100, 25.0000));
    deepForestJunkItemList
        .add(JunkItem("Black wood [Uncommon]", true, 1, 45, 100.00));

    Location deepForest = Location(5, "Deep Forset [Lv.13-24]",
        deepForestConnection, 13, false, deepForestJunkItemList);
    locationList.add(deepForest);

    //Tower
    List<String> towerConnection = [];
    towerConnection.add("Deep Forset [Lv.13-24]");

    List<Item> towerJunkItemList = [];
    towerJunkItemList
        .add(Item("Magic crystal [Legendary]", true, 1, 500, 0.3500));
    towerJunkItemList.add(Item("Magic crystal [Epic]", true, 1, 500, 1.55));
    towerJunkItemList.add(Item("Magic crystal [Rare]", true, 1, 250, 2.500));
    towerJunkItemList
        .add(JunkItem("Noble's accessory [Uncommon]", true, 1, 200, 5.5000));
    towerJunkItemList
        .add(JunkItem("Gold coin [Uncommon]", true, 1, 100, 100.00));

    Location tower = Location(
        6, "Tower [Lv.25+]", towerConnection, 25, false, towerJunkItemList);
    locationList.add(tower);

    //Capital
    List<String> capitalConnection = [];
    capitalConnection.add("Deep Forset [Lv.13-24]");
    Location capital = Location(7, "Capital", capitalConnection, 22, true, []);
    locationList.add(capital);

    Map map = Map(locationList);
    return map;
  }
}

class Map {
  List<Location> locationList = [];

  Map(this.locationList);

  void playerGoToLocation(String input, Player player) {
    int number = int.parse(input);
    for (Location location in locationList) {
      if (location.name == player.currentLocation) {
        for (String connectLocation in location.connectLocation) {
          number--;
          if (number == 0) {
            Location? wantToGolocation = getLocation(connectLocation);
            if (wantToGolocation!.canGoTo(player.level)) {
              player.changeLocation(wantToGolocation);
            }
            return;
          }
        }
      }
    }
    return;
  }

  Location? getLocation(String locationName) {
    for (Location location in locationList) {
      if (locationName == location.name) {
        return location;
      }
    }
    return null;
  }
}

class Location {
  late int id;
  late String name;
  late List<String> connectLocation;
  late int levelReq;
  late bool hShop;
  late List<Item> junkItemList;

  Location(this.id, this.name, this.connectLocation, this.levelReq, this.hShop,
      this.junkItemList);

  bool hasShop() {
    return hShop;
  }

  List<Item> grindDrop() {
    List<Item> dropList = [];
    int rand = Random().nextInt(1000000) + 1;
    for (Item item in junkItemList) {
      if (rand <= item.dropRateNum) {
        print("Get ${item.name} x${item.amount}");
        dropList.add(item);
        break;
      }
    }
    rand = Random().nextInt(1000000) + 1;
    if (rand >= 500000) {
      rand = Random().nextInt(1000000) + 1;
      for (Item item in junkItemList) {
        if (rand <= item.dropRateNum) {
          print("Get ${item.name} x${item.amount}");
          dropList.add(item);
          break;
        }
      }
    }
    while (true) {
      rand = Random().nextInt(1000000) + 1;
      if (rand >= 10000) {
        break;
      }
      rand = Random().nextInt(1000000) + 1;
      for (Item item in junkItemList) {
        if (rand <= item.dropRateNum) {
          print("Get ${item.name} x${item.amount}");
          dropList.add(item);
          break;
        }
      }
    }
    return dropList;
  }

  bool hasGrind() {
    return junkItemList.isNotEmpty;
  }

  void showConnectLocation() {
    int count = 1;
    for (String locationName in connectLocation) {
      print("$count. $locationName");
      count++;
    }
    print("$count. Back");
  }

  bool canGoTo(int playerLevel) {
    if (playerLevel >= levelReq) {
      return true;
    }
    print("----------------------------------------------");
    print("    Need to be [Lv.$levelReq] to access this area.");
    print("----------------------------------------------");
    return false;
  }
}
