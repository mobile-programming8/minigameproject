import 'dart:io';
import 'dart:math';
import 'gameMap.dart';

import 'monster.dart';
import 'player.dart';

bool isGameFinish = false;

void start() {
  while (!isGameFinish) {
    showMenu();
    selectMenu();
  }
}

void selectMenu() {
  String? input = stdin.readLineSync();
  switch (input) {
    case "1":
      newGame();
      break;
    case "2":
      //loadGame();
      isGameFinish = true;
      break;
    case "3":
      isGameFinish = true;
      break;
    default:
      isGameFinish = true;
  }
}

void newGame() {
  print("Enter your name.");
  String? name = stdin.readLineSync();
  print("Select a class.");
  print("1.Swordsman");
  print("2.Wizard");
  print("3.Archer");
  print("4.Cleric");
  String? selectedClass = stdin.readLineSync();
  switch (selectedClass) {
    case "1":
      Player player = Player(name!, 1, 100, 50, 2, 1);
      player.addItem(Weapon("Wooden Sword", "Sword", 3, 0));
      gameStart(player);
      break;
    case "2":
      Player player = Player(name!, 2, 80, 150, 1, 1);
      player.addItem(Weapon("Wooden Staff", "Staff", 5, 0));
      gameStart(player);
      break;
    case "3":
      Player player = Player(name!, 3, 90, 105, 3, 1);
      player.addItem(Weapon("Wooden Bow", "Bow", 5, 0));
      gameStart(player);
      break;
    case "4":
      Player player = Player(name!, 4, 65, 120, 1, 4);
      player.addItem(Weapon("Wooden Mace", "Mace", 2, 0));
      gameStart(player);
      break;
    default:
      break;
  }
}

void gameStart(Player player) {
  while (!isGameFinish) {
    player.showInfo();
    gameMenu(player);
  }
}

void cheat(Player player) {
  player.baseAP = 9999999999;
}

void gameMenu(Player player) {
  print("1. Map");
  if (player.locationHasShop()) {
    print("2. Shop");
  } else {
    print("2. Explore");
  }
  print("3. Inventory");
  int count = 4;
  if (player.skillPoint > 0) {
    print("$count. Learn or upgrade skill");
    count++;
  }
  print("$count. Exit");
  String? input = stdin.readLineSync();
  switch (input) {
    case "1":
      showMap(player);
      break;
    case "2":
      if (player.locationHasShop()) {
        showShop(player);
      } else {
        explore(player);
      }
      break;
    case "3":
      inventoryMenu(player);
      break;
    case "4":
      if (player.skillPoint > 0) {
        print("1. Upgrade skill");
        print("2. Learn skill");
        print("3. Back");
        String input = stdin.readLineSync()!;
        if (input == "1") {
          if (upskillMenu(player)) {
            player.skillPoint--;
          }
        }
        if (input == "2") {
          if (learnSkillMenu(player)) {
            player.skillPoint--;
          }
        }
        break;
      }
      isGameFinish = true;
      break;
    case "5":
      if (player.skillPoint > 0) {
        isGameFinish = true;
      }
      break;
    default:
  }
}

bool learnSkillMenu(Player player) {
  player.showUnLearnSkill();
  String input = stdin.readLineSync()!;
  int skillNum = int.parse(input);
  if (player.learnSkill(skillNum)) {
    return true;
  }
  return false;
}

bool upskillMenu(Player player) {
  player.showSkill();
  String input = stdin.readLineSync()!;
  int skillNum = int.parse(input);
  if (player.upskill(skillNum)) {
    return true;
  }
  return false;
}

void explore(Player player) {
  while (true) {
    print("1. Fight monster");
    print("2. Grind for item");
    print("3. Back");
    String? input = stdin.readLineSync();
    switch (input) {
      case "1":
        fight(player);
        break;
      case "2":
        grind(player);
        break;
      case "3":
        return;
      default:
    }
  }
}

void grind(Player player) {
  print("To exit grind type \"quit\"");
  while (true) {
    String ch = "";
    for (var i = 0; i < player.currentLocationID; i++) {
      var intValue = Random().nextInt(93) + 33;
      String tmp = String.fromCharCode(intValue);
      ch = ch + tmp;
    }
    print("To grind click : $ch");
    String? input = stdin.readLineSync();
    if (input == ch) {
      List<Item> drop = player.getLocation()!.grindDrop();
      for (Item item in drop) {
        player.addItem(item);
      }
    } else if (input == "quit") {
      return;
    }
    print("To exit grind type \"quit\"");
  }
}

void fight(Player player) {
  Monster monster = monsterGenerator(player);
  fightSystem(monster, player);
  print("<<<Fight ended>>>");
  player.resetSkillCD();
  sleep(Duration(seconds: 1));
}

Monster monsterGenerator(Player player) {
  if (player.currentLocationID == 1) {
    // Forest
    if (player.level < 5) {
      return Slime(Random().nextInt(player.level) + 1);
    } else {
      int rand = Random().nextInt(2) + 1;
      if (rand == 2) {
        return MotherSlime(Random().nextInt(5) + 1);
      } else {
        return Slime(Random().nextInt(5) + 1);
      }
    }
  }
  if (player.currentLocationID == 2) {
    // Mountain [Lv.6-10]
    int rand = Random().nextInt(2) + 1;
    if (rand == 1) {
      return Goblin(Random().nextInt(5) + 6);
    } else {
      return GoblinLord(Random().nextInt(3) + 8);
    }
  }
  return SlimeGod(99999);
}

void fightSystem(Monster monster, Player player) {
  player.randomPlayerTurn;
  print("Encounter!!!");
  print("");
  sleep(Duration(seconds: 1));
  while (monster.currentHP > 0 || player.currentHP! > 0) {
    monster.showInfo();
    print(
        "[${player.name}] Lv.${player.level} HP ${player.currentHP}/${player.baseHP}");
    if (player.isPlayerTurn == 0) {
      sleep(Duration(seconds: 1));
      print("");
      print("Player Turn");
      player.skillCDController();
      player.playerShowAction();
      String? input = stdin.readLineSync();
      switch (input) {
        case "1":
          attack(player, monster);
          break;
        case "2":
          if (useSkillMenu(monster, player)) {
            player.isPlayerTurn++;
          }
          break;
        case "3":
          if (useItemMenu(player)) {
            player.isPlayerTurn++;
          }
          break;
        case "4":
          int rand = Random().nextInt(100) + 1;
          if (rand > 75) {
            print("Flee successfully");
            return;
          }
          print("Failed to flee");
          player.isPlayerTurn++;
          break;
        default:
      }
    }
    if (monster.currentHP <= 0) {
      print("${monster.name} died");
      sleep(Duration(seconds: 1));
      print("(+${monster.moneyValue}) Gold");
      player.money += monster.moneyValue;
      double expCal = monster.expValue / (player.level / monster.level);
      int totalExpValue = expCal.toInt();
      player.addEXP(totalExpValue);
      break;
    }
    if (player.isPlayerTurn > 0) {
      sleep(Duration(seconds: 1));
      print("");
      print("Monster Turn");
      monster.autoAction(player);
      player.isPlayerTurn--;
    }
    if (player.currentHP! <= 0) {
      print("You lose");
      sleep(Duration(seconds: 1));
      print("You will revive with half HP left.");
      sleep(Duration(seconds: 1));
      double playerHPcal = player.baseHP / 2;
      player.currentHP = playerHPcal.toInt();
      break;
    }
  }
}

void attack(Player player, Monster monster) {
  if (player.mainHand?.type == "Bow" || player.offHand?.type == "Bow") {
    bool flag = false;
    for (Item item in player.inventory) {
      if (item is Ammunition) {
        if (item.amount > 1) {
          item.amount--;
          if (item.amount == 1) {
            print("You have ${item.amount} ${item.name} left.");
          } else {
            print("You have ${item.amount} ${item.name}s left.");
          }
          flag = true;
          break;
        } else {
          player.inventory.remove(item);
          print("You ran out of arrow.");
          flag = true;
          break;
        }
      }
    }
    if (flag) {
      int totalDMG = player.getTotalAP();
      monster.currentHP -= totalDMG;
      if (totalDMG > 1) {
        print("${player.name} Deals $totalDMG Damage!");
      } else {
        print("${player.name} Deal $totalDMG Damage!");
      }
      player.isPlayerTurn++;
      return;
    } else {
      print("You don't have arrow.");
      int totalDMG = player.baseAP;
      monster.currentHP -= totalDMG;
      if (totalDMG > 1) {
        print("${player.name} Deals $totalDMG Damage!");
      } else {
        print("${player.name} Deal $totalDMG Damage!");
      }
      player.isPlayerTurn++;
      return;
    }
  } else {
    int totalDMG = player.getTotalAP();
    monster.currentHP -= totalDMG;
    if (totalDMG > 1) {
      print("${player.name} Deals $totalDMG Damage!");
    } else {
      print("${player.name} Deal $totalDMG Damage!");
    }
    player.isPlayerTurn++;
    return;
  }
}

bool useSkillMenu(Monster monsterTarget, Player player) {
  player.showSkill();
  String? input = stdin.readLineSync();
  if (useSkill(monsterTarget, player, input)) {
    return true;
  }
  return false;
}

bool useSkill(Monster monsterTarget, Player player, String? input) {
  int skillNumber = int.parse(input!);
  for (Skill skill in player.skillTree) {
    skillNumber--;
    if (skillNumber == 0) {
      if (!skill.isCoolDown()) {
        if (player.currentMP! >= skill.MPcost) {
          if (skill is HealingSkill) {
            HealingSkill healingSkill = skill;
            healingSkill.useSkill(monsterTarget, player);
            return true;
          } else {
            //attack skill
            skill.useSkill(monsterTarget, player);
            return true;
          }
        } else {
          print("You not have enough MP.");
          return false;
        }
      } else {
        print("Skill is cooldown.");
        return false;
      }
    }
  }
  return false;
}

void inventoryMenu(Player player) {
  while (true) {
    player.showInventory(false);
    print("1.Equip weapon");
    print("2.Use item");
    print("3.Back");
    String? input = stdin.readLineSync();
    switch (input) {
      case "1":
        equipWeaponMenu(player);
        break;
      case "2":
        while (useItemMenu(player));
        break;
      case "3":
        return;
      default:
        return;
    }
  }
}

bool useItemMenu(Player player) {
  if (player.haveItemToUse()) {
    String? input = stdin.readLineSync();
    if (player.useItem(input, player)) {
      return true;
    }
    return false;
  } else {
    print("You don't have any consumable item");
    return false;
  }
}

void equipWeaponMenu(Player player) {
  while (true) {
    if (player.mainHand == null) {
      print("1.Mainhand : none");
    } else {
      print("1.Mainhand : ${player.mainHand!.name}");
    }
    if (player.offHand == null) {
      print("2.Offhand : none");
    } else {
      print("2.Offhand : ${player.offHand!.name}");
    }
    print("3.Back");
    String? input = stdin.readLineSync();
    switch (input) {
      case "1":
        if (player.showWeaponInInventory()) {
          String? input = stdin.readLineSync();
          player.equipMainhand(input);
        }
        break;
      case "2":
        if (player.showWeaponInInventory()) {
          String? input = stdin.readLineSync();
          player.equipOffhand(input);
        }
        break;
      case "3":
        return;
      default:
        return;
    }
  }
}

void showShop(Player player) {
  print("1. Buy");
  print("2. Sell");
  print("3. Back");
  String? input = stdin.readLineSync();
  switch (input) {
    case "1":
      while (true) {
        print("Money :${player.money} Gold");
        switch (player.currentLocationID) {
          //BeginTown shop
          case 0:
            print("---------------------------------------");
            print("1.  HP potion    (+HP 150) :   200 Gold");
            print("2.  MP potion    (+MP 50)  :   250 Gold");
            print("3.  Wooden Sword (+ATK 3)  :   500 Gold");
            print("4.  Wooden Bow   (+ATK 5)  :   500 Gold");
            print("5.  Wooden Staff (+ATK 5)  :   500 Gold");
            print("6.  Wooden Mace  (+ATK 2)  :   500 Gold");
            print("7.  Iron Sword   (+ATK 26) : 2,500 Gold");
            print("8.  Elder Bow    (+ATK 30) : 2,500 Gold");
            print("9.  Mage Staff   (+ATK 30) : 2,500 Gold");
            print("10. Priest Mace  (+ATK 24) : 2,500 Gold");
            print("11. Arrow x 10             :   100 Gold");
            print("12. Back");
            print("---------------------------------------");
            String? input = stdin.readLineSync();
            switch (input) {
              case "1":
                if (player.money >= 200) {
                  player.money -= 200;
                  player.addItem(HealingPotion("HP potion", 150, 1, 50));
                } else {
                  print("Not enough money.");
                }
                break;
              case "2":
                if (player.money >= 250) {
                  player.money -= 250;
                  player.addItem(MagicPotion("MP potion", 50, 1, 62));
                } else {
                  print("Not enough money.");
                }
                break;
              case "3":
                if (player.money >= 500) {
                  player.money -= 500;
                  player.addItem(Weapon("Wooden Sword", "Sword", 3, 125));
                } else {
                  print("Not enough money.");
                }
                break;
              case "4":
                if (player.money >= 500) {
                  player.money -= 500;
                  player.addItem(Weapon("Wooden Bow", "Bow", 5, 125));
                } else {
                  print("Not enough money.");
                }
                break;
              case "5":
                if (player.money >= 500) {
                  player.money -= 500;
                  player.addItem(Weapon("Wooden Staff", "Staff", 5, 125));
                } else {
                  print("Not enough money.");
                }
                break;
              case "6":
                if (player.money >= 500) {
                  player.money -= 500;
                  player.addItem(Weapon("Wooden Mace", "Mace", 2, 125));
                } else {
                  print("Not enough money.");
                }
                break;
              case "7":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Iron Sword", "Sword", 26, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "8":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Elder Bow", "Bow", 30, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "9":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Mage Staff", "Staff", 30, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "10":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Priest Mace", "Mace", 24, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "11":
                if (player.money >= 100) {
                  player.money -= 100;
                  player.addItem(Ammunition("Arrow", 1, 10, 1));
                } else {
                  print("Not enough money.");
                }
                break;
              case "12":
                return;
              default:
                return;
            }
            break;
          case 3: //midtown
            print("---------------------------------------");
            print("1.  High HP potion    (+HP 500) :   600 Gold");
            print("2.  High MP potion    (+MP 250) :   750 Gold");
            print("3.  Iron Sword        (+ATK 26) : 2,500 Gold");
            print("4.  Elder Bow         (+ATK 30) : 2,500 Gold");
            print("5.  Mage Staff        (+ATK 30) : 2,500 Gold");
            print("6.  Priest Mace       (+ATK 24) : 2,500 Gold");
            print("7.  Dark Blade        (+ATK 56) : 8,500 Gold");
            print("8.  Dark Bow          (+ATK 60) : 8,500 Gold");
            print("9.  Dark Staff        (+ATK 60) : 8,500 Gold");
            print("10. High Priest Staff (+ATK 54) : 8,500 Gold");
            print("11. Sharp Arrow x 10            : 1,100 Gold");
            print("12. Back");
            print("---------------------------------------");
            String? input = stdin.readLineSync();
            switch (input) {
              case ("1"):
                if (player.money >= 600) {
                  player.money -= 600;
                  player.addItem(HealingPotion("High HP potion", 500, 1, 150));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("2"):
                if (player.money >= 750) {
                  player.money -= 750;
                  player.addItem(MagicPotion("High MP potion", 250, 1, 170));
                } else {
                  print("Not enough money.");
                }
                break;
              case "3":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Iron Sword", "Sword", 26, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "4":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Elder Bow", "Bow", 30, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "5":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Mage Staff", "Staff", 30, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case "6":
                if (player.money >= 2500) {
                  player.money -= 2500;
                  player.addItem(Weapon("Priest Mace", "Mace", 24, 600));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("7"):
                if (player.money >= 8500) {
                  player.money -= 8500;
                  player.addItem(Weapon("Dark Blade", "Sword", 56, 2000));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("8"):
                if (player.money >= 8500) {
                  player.money -= 8500;
                  player.addItem(Weapon("Dark Bow", "Bow", 60, 2000));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("9"):
                if (player.money >= 8500) {
                  player.money -= 8500;
                  player.addItem(Weapon("Dark Staff", "Staff", 60, 2000));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("10"):
                if (player.money >= 8500) {
                  player.money -= 8500;
                  player.addItem(Weapon("High Priest Staff", "Mace", 60, 2000));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("11"):
                if (player.money >= 1100) {
                  player.money -= 1100;
                  player.addItem(Ammunition("Sharp Arrow", 1.1, 10, 1));
                } else {
                  print("Not enough money.");
                }
                break;
              case ("12"):
                return;
              default:
                return;
            }
            break;
          case 7: //capital
            print("!!!Work In Progress!!!");
            break;
          //next shop
          default:
            print("!!!Work In Progress!!!");
            break;
        }
      }
    case "2":
      while (player.sellItem());
      break;
    case "3":
      return;
    default:
      return;
  }
}

void showMap(Player player) {
  Map map = GameMap.getMainGameMap();
  for (Location location in map.locationList) {
    if (player.currentLocationID == location.id) {
      location.showConnectLocation();
    }
  }
  String? input = stdin.readLineSync();
  map.playerGoToLocation(input!, player);
}

// void showMap(Player player) {
//   for (var i = player.currentLocationID; i > 0; i--) {
//     stdout.write("            ");
//   }
//   print("     vvv");
//   print(
//       "  BeginTown  >   Forest    >   MidTown   >  Mountain   > Capital City");
//   if (player.currentLocationID == 0) {
//     print("1.Go right");
//     String? input = stdin.readLineSync();
//     switch (input) {
//       case "1":
//         player.changeLocation(player.currentLocationID + 1);
//         break;
//       default:
//     }
//   } else if (player.currentLocationID == 4) {
//     print("1.Go left");
//     String? input = stdin.readLineSync();
//     switch (input) {
//       case "1":
//         player.changeLocation(player.currentLocationID - 1);
//         break;
//       default:
//     }
//   } else {
//     print("1.Go left");
//     print("2.Go right");
//     String? input = stdin.readLineSync();
//     switch (input) {
//       case "1":
//         player.changeLocation(player.currentLocationID - 1);
//         break;
//       case "2":
//         player.changeLocation(player.currentLocationID + 1);
//         break;
//       default:
//     }
//   }
//   for (var i = player.currentLocationID; i > 0; i--) {
//     stdout.write("           >");
//   }
//   print("    vvv");
//   print(" BeginTown >  Forest   >  MidTown  > Mountain  >CapitalCity");
// }

void loadGame() {
  print("!!!Work In Progress!!!");
  //future improve
}

void showMenu() {
  print("1.New Game");
  print("2.Exit");
}
