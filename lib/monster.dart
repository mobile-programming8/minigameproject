import 'dart:math';

import 'player.dart';

abstract class Monster {
  late String name;
  late int HP;
  late int currentHP;
  late int MP;
  late int level;
  late int expValue;
  late int moneyValue;

  Monster(
      this.name, this.HP, this.MP, this.level, this.expValue, this.moneyValue) {
    currentHP = HP;
  }

  void showInfo() {
    print("[$name] Lv.$level HP $currentHP/$HP");
  }

  void autoAction(Player player) {}

  void basicAttack(Player player) {}
}

class Slime extends Monster {
  Slime(int level)
      : super("Slime", 10 + (5 * (level - 1)), 5 + (5 * (level - 1)), level,
            25 + (2 * (level)), Random().nextInt(12) + 1);

  @override
  void autoAction(Player player) {
    while (true) {
      int intRand = Random().nextInt(2) + 1;
      if (intRand == 1) {
        basicAttack(player);
        return;
      } else {
        if (MP >= 5) {
          slimeSkill(player);
          return;
        }
      }
    }
  }

  @override
  void basicAttack(Player player) {
    int totalDMG = 10 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    print("$name basic attack! (-$totalDMG)");
    player.currentHP = player.currentHP! - totalDMG;
  }

  void slimeSkill(Player player) {
    MP -= 5;
    int totalDMG = 15 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    print("$name skill attack! (-$totalDMG)");
    player.currentHP = player.currentHP! - totalDMG;
  }

  @override
  void showInfo() {
    print("[$name] Lv.$level HP $currentHP/$HP");
  }
}

class MotherSlime extends Slime {
  int glueSkillCD = 0;

  MotherSlime(int level) : super(level) {
    name = "Mother Slime";
    MP += 100;
  }

  @override
  void autoAction(Player player) {
    while (true) {
      int intRand = Random().nextInt(3) + 1;
      if (intRand == 1) {
        basicAttack(player);
        if (glueSkillCD > 0) {
          glueSkillCD--;
        }
        return;
      } else if (intRand == 2) {
        if (MP >= 5) {
          slimeSkill(player);
          if (glueSkillCD > 0) {
            glueSkillCD--;
          }
          return;
        }
      } else {
        if (MP >= 30 && glueSkillCD == 0) {
          glue(player);
          return;
        }
      }
    }
  }

  void glue(Player player) {
    MP -= 30;
    int totalDMG = 15 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    print("$name use [Glue] skill! (-$totalDMG)");
    if ((Random().nextInt(100) + 1) <= 50) {
      player.isPlayerTurn++;
      print("You are stunned!!! (-1 Player Turn)");
    }
    glueSkillCD += 3; //CD 3 turns
    player.currentHP = player.currentHP! - totalDMG;
  }
}

class SlimeGod extends Slime {
  SlimeGod(int level) : super(level);

  @override
  void autoAction(Player player) {
    basicAttack(player);
    return;
  }

  @override
  void basicAttack(Player player) {
    int totalDMG = 999999999999999999;
    print("$name basic attack! (-$totalDMG)");
    player.currentHP = player.currentHP! - totalDMG;
  }
}

class Goblin extends Monster {
  int axeThrowCD = 0;

  Goblin(int level)
      : super("Goblin", 50 + 15 * level, 25 + 5 * level, level,
            25 + (5 * (level)), Random().nextInt(24) + 9);

  @override
  void basicAttack(Player player) {
    int totalDMG = 25 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    print("$name basic attack! (-$totalDMG)");
    player.currentHP = player.currentHP! - totalDMG;
    if (axeThrowCD > 0) {
      axeThrowCD--;
    }
  }

  @override
  void autoAction(Player player) {
    while (true) {
      int intRand = Random().nextInt(3) + 1;
      if (intRand >= 2) {
        basicAttack(player);
        return;
      } else {
        if (axeThrowCD == 0 && MP >= 25) {
          skillAxeThrow(player);
          return;
        }
      }
    }
  }

  void skillAxeThrow(Player player) {
    MP -= 25;
    int totalDMG = 25 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    if (Random().nextInt(100) + 1 < 25) {
      totalDMG *= 2;
      print("$name use [Axe Throw] skill! (-$totalDMG) Critical!");
    } else {
      print("$name use [Axe Throw] skill! (-$totalDMG)");
    }
    axeThrowCD += 3; //CD 3 turns
    player.currentHP = player.currentHP! - totalDMG;
  }
}

class GoblinLord extends Goblin {
  int warCryCD = 0;
  int warCryBuffDuration = 0;

  GoblinLord(int level) : super(level) {
    name = "[Boss] Goblin Lord";
    HP += 1000;
    currentHP = HP;
    MP += 500;
  }

  @override
  void autoAction(Player player) {
    while (true) {
      int intRand = Random().nextInt(10) + 1;
      if (intRand == 1) {
        if (MP >= 100 && warCryCD == 0) {
          skillWarCry();
          if (axeThrowCD > 0) {
            axeThrowCD--;
          }
          return;
        }
      } else if (intRand >= 2 && intRand <= 4) {
        if (MP >= 25 && axeThrowCD == 0) {
          skillAxeThrow(player);
          if (warCryCD > 0) {
            warCryCD--;
          }
          return;
        }
      } else {
        basicAttack(player);
        if (axeThrowCD > 0) {
          axeThrowCD--;
        }
        if (warCryCD > 0) {
          warCryCD--;
        }
        return;
      }
    }
  }

  void skillWarCry() {
    MP -= 100;
    warCryCD = 5;
    warCryBuffDuration = 3;
    print("$name use [War Cry] skill! (Increase damage dealt 50% for 3 turns)");
  }

  @override
  void basicAttack(Player player) {
    int totalDMG = 25 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    totalDMG = warCryDamageBuffCal(totalDMG);
    print("$name basic attack! (-$totalDMG)");
    player.currentHP = player.currentHP! - totalDMG;
    if (axeThrowCD > 0) {
      axeThrowCD--;
    }
  }

  @override
  void skillAxeThrow(Player player) {
    MP -= 25;
    int totalDMG = 25 - player.baseDP;
    if (totalDMG < 0) {
      totalDMG = 0;
    }
    totalDMG = warCryDamageBuffCal(totalDMG);
    if (Random().nextInt(100) + 1 < 25) {
      totalDMG *= 2;
      print("$name use [Axe Throw] skill! (-$totalDMG) Critical!");
    } else {
      print("$name use [Axe Throw] skill! (-$totalDMG)");
    }
    axeThrowCD += 3; //CD 3 turns
    player.currentHP = player.currentHP! - totalDMG;
  }

  int warCryDamageBuffCal(int totalDMG) {
    if (warCryBuffDuration > 0) {
      warCryBuffDuration--;
      double tempDMGcal = totalDMG * 1.5;
      totalDMG = tempDMGcal.round();
    }
    return totalDMG;
  }

  @override
  void showInfo() {
    if (warCryBuffDuration != 0) {
      if (warCryBuffDuration == 1) {
        print(
            "[$name] Lv.$level HP $currentHP/$HP [War Cry $warCryBuffDuration turn]");
        return;
      } else {
        print(
            "[$name] Lv.$level HP $currentHP/$HP [War Cry $warCryBuffDuration turns]");
        return;
      }
    }
    print("[$name] Lv.$level HP $currentHP/$HP");
    return;
  }
}
