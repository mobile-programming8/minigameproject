import 'dart:io';
import 'dart:math';

import 'gameMap.dart';
import 'monster.dart';

class Player {
  late String name;
  //status
  int level = 1;
  int currentEXP = 0;
  double expExponent = 1.15;
  int expToLevelUp = 100;
  int classID = 0;
  int baseHP = 100;
  int baseMP = 50;
  int baseAP = 1;
  int baseDP = 1;
  int skillPoint = 0;

  //current status
  int? currentHP;
  int? currentMP;

  //inventory
  List<Item> inventory = [];

  //equipment
  Weapon? mainHand = null;
  Weapon? offHand = null;

  //currency
  int money = 1000;

  //location
  int currentLocationID = 0;
  String currentLocation = "Beginner Town";

  //fight system
  int isPlayerTurn = 0;

  //skillTree
  List<Skill> skillTree = [];
  List<Skill> unLearnSkill = [];

  void randomPlayerTurn() {
    isPlayerTurn = Random().nextInt(2); //0 is player turn
  }

  void changeLocation(Location location) {
    currentLocationID = location.id;
    currentLocation = location.name;
  }

  void addItem(Item item) {
    if (item.stackable) {
      for (Item tmp in inventory) {
        if (tmp.name == item.name) {
          tmp.amount = tmp.amount + item.amount;
          return;
        }
      }
      inventory.add(item);
      return;
    } else {
      inventory.add(item);
      return;
    }
  }

  bool haveItemToUse() {
    int itemNumber = 1;
    for (Item item in inventory) {
      if (item is Consumable) {
        print("$itemNumber. ${item.name} x ${item.amount}");
        itemNumber++;
      }
    }
    print("$itemNumber. Back");
    if (itemNumber != 1) {
      return true;
    }
    return false;
  }

  void showInventory(bool withPrice) {
    sortInventory();
    if (withPrice) {
      if (inventory.isNotEmpty) {
        int count = 1;
        for (Item item in inventory) {
          if (item.stackable) {
            print(
                "$count. ${item.name} x ${item.amount}\n   Price : ${item.sellprice}");
            count++;
          } else {
            if (item is Weapon) {
              Weapon weapon = item;
              if (canEquip(weapon)) {
                if (weapon == mainHand || weapon == offHand) {
                  //print("${item.name} x 1 [Equipped]");
                } else {
                  print(
                      "$count. ${item.name} x 1 [Equipable]\n   Price : ${item.sellprice}");
                  count++;
                }
              } else {
                if (weapon.isEquiped) {
                  //print("${item.name} x 1 [Equipped]");
                } else {
                  print(
                      "$count. ${item.name} x 1 [Unequipable]\n   Price : ${item.sellprice}");
                  count++;
                }
              }
            } else {
              print("$count. ${item.name} x 1\nPrice :${item.sellprice}");
              count++;
            }
          }
        }
        print("$count. Back");
      } else {
        print("Inventory is Empty");
      }
    } else {
      if (inventory.isNotEmpty) {
        for (Item item in inventory) {
          if (item.stackable) {
            print("${item.name} x ${item.amount}");
          } else {
            if (item is Weapon) {
              Weapon weapon = item;
              if (canEquip(weapon)) {
                if (weapon == mainHand || weapon == offHand) {
                  print("${item.name} x 1 [Equipped]");
                } else {
                  print("${item.name} x 1 [Equipable]");
                }
              } else {
                if (weapon.isEquiped) {
                  print("${item.name} x 1 [Equipped]");
                } else {
                  print("${item.name} x 1 [Unequipable]");
                }
              }
            } else {
              print("${item.name} x 1");
            }
          }
        }
      } else {
        print("Inventory is Empty");
      }
    }
  }

  Player(this.name, this.classID, this.baseHP, this.baseMP, this.baseAP,
      this.baseDP) {
    currentHP = baseHP;
    currentMP = baseMP;
    expToLevelUp = expToLevelUpCalculate();
    skillTree = getSkillTree();
    unLearnSkill = getUnLearnSkill();
  }

  int getLevel() {
    return level;
  }

  String getClass() {
    switch (classID) {
      case 0:
        return "Novice";
      case 1:
        return "Swordsman";
      case 2:
        return "Wizard";
      case 3:
        return "Archer";
      case 4:
        return "Cleric";
      default:
        return "null";
    }
  }

  int getBaseHP() {
    return baseHP;
  }

  int getBaseMP() {
    return baseMP;
  }

  int getBaseAP() {
    return baseAP;
  }

  int getBaseDP() {
    return baseDP;
  }

  void showInfo() {
    print(
        "Name :$name | Level :$level | EXP :$currentEXP/${expToLevelUpCalculate()} | Money :$money Gold");
    print(
        "HP : $currentHP/$baseHP | MP : $currentMP/$baseMP | Current location : $currentLocation");
    if (mainHand != null) {
      print("Mainhand : ${mainHand!.name} (+ATK ${mainHand!.attackPower})");
    }
    if (offHand != null) {
      print("Offhand  : ${offHand!.name} (+ATK ${offHand!.attackPower})");
    }
  }

  int expToLevelUpCalculate() {
    double tmp = (4 * (level * level * level)) / 5;
    return tmp.round();
  }

  bool canEquip(Weapon weapon) {
    if (!weapon.isEquiped) {
      switch (classID) {
        case 0:
          return false;
        case 1:
          if (weapon.type == "Sword") {
            return true;
          }
          return false;
        case 2:
          if (weapon.type == "Staff") {
            return true;
          }
          return false;
        case 3:
          if (weapon.type == "Bow") {
            return true;
          }
          return false;
        case 4:
          if (weapon.type == "Mace") {
            return true;
          }
          return false;
        default:
          return false;
      }
    }
    return false;
  }

  bool showWeaponInInventory() {
    if (inventory.isNotEmpty) {
      int itemNumber = 1;
      for (Item item in inventory) {
        if (item is Weapon) {
          Weapon weapon = item;
          if (canEquip(weapon)) {
            print("$itemNumber. ${weapon.name}");
            itemNumber++;
          }
        }
      }
      print("$itemNumber. none");
      itemNumber++;
      print("$itemNumber. Back");
      return true;
    } else {
      print("Your inventory is empty.");
      return false;
    }
  }

  void equipMainhand(String? input) {
    int weaponNumber = int.parse(input!);
    for (Item item in inventory) {
      if (item is Weapon) {
        Weapon weapon = item;
        if (canEquip(weapon)) {
          weaponNumber--;
          if (weaponNumber == 0) {
            unequipMainHand();
            weapon.isEquiped = true;
            mainHand = weapon;
            return;
          }
        }
      }
    }
    if (weaponNumber == 1) {
      unequipMainHand();
    }
  }

  void unequipMainHand() {
    for (Item item in inventory) {
      if (item is Weapon) {
        Weapon weapon = item;
        if (weapon == mainHand && weapon.isEquiped) {
          weapon.isEquiped = false;
          mainHand = null;
        }
      }
    }
  }

  void equipOffhand(String? input) {
    int weaponNumber = int.parse(input!);
    for (Item item in inventory) {
      if (item is Weapon) {
        Weapon weapon = item;
        if (canEquip(weapon)) {
          weaponNumber--;
          if (weaponNumber == 0) {
            unequipOffHand();
            weapon.isEquiped = true;
            offHand = weapon;
            return;
          }
        }
      }
    }
    if (weaponNumber == 1) {
      unequipOffHand();
    }
  }

  void unequipOffHand() {
    for (Item item in inventory) {
      if (item is Weapon) {
        Weapon weapon = item;
        if (weapon == offHand && weapon.isEquiped) {
          weapon.isEquiped = false;
          offHand = null;
        }
      }
    }
  }

  void addEXP(int getExpValue) {
    print("(+$getExpValue) EXP");
    currentEXP += getExpValue;
    while (currentEXP >= expToLevelUp) {
      currentEXP -= expToLevelUp;
      levelIncrease();
      expToLevelUp = expToLevelUpCalculate();
      print("Level : $level");
      print("EXP $currentEXP/$expToLevelUp");
    }

    // if (currentEXP >= expToLevelUp) {
    //   currentEXP -= expToLevelUp;
    //   print("Level Up!!!");
    //   levelIncrease();
    //   expToLevelUp = expToLevelUpCalculate();
    //   print("Level : $level");
    //   print("EXP $currentEXP/$expToLevelUp");
    //   return;
    // }else{
    //   print("Level : $level");
    //   print("EXP $currentEXP/$expToLevelUp");
    // }
  }

  void levelIncrease() {
    level++;
    if (level < 100) {
      double tmp = baseHP * 1.05;
      print("HP $baseHP >>> ${tmp.round()}");
      baseHP = tmp.round();
      currentHP = baseHP;
      tmp = baseMP * 1.04;
      print("MP $baseMP >>> ${tmp.round()}");
      baseMP = tmp.round();
      currentMP = baseMP;
      print("Base AP $baseAP >>> ${baseAP + 1}");
      baseAP += 1;
      print("Base DP $baseDP >>> ${baseDP + 1}");
      baseDP += 1;
      skillPoint++;
      return;
    }
    double tmp = baseHP * 1.005;
    print("HP $baseHP >>> ${tmp.round()}");
    baseHP = tmp.round();
    currentHP = baseHP;
    tmp = baseMP * 1.004;
    print("MP $baseMP >>> ${tmp.round()}");
    baseMP = tmp.round();
    currentMP = baseMP;
    print("Base AP $baseAP >>> ${baseAP + 1}");
    baseAP += 1;
    print("Base DP $baseDP >>> ${baseDP + 1}");
    baseDP += 1;
    skillPoint++;
    return;
  }

  void playerShowAction() {
    print("1.Normal Attack");
    print("2.Skill");
    print("3.Use item");
    print("4.Flee");
  }

  bool useItem(String? input, Player player) {
    int itemNumber = int.parse(input!);
    for (Item item in inventory) {
      if (item is Consumable) {
        itemNumber--;
        if (itemNumber == 0) {
          Consumable con = item;
          con.use(player);
          if (item.amount > 1) {
            item.amount--;
          } else {
            inventory.remove(item);
          }
          return true;
        }
      }
    }
    return false;
  }

  Location? getLocation() {
    Map map = GameMap.getMainGameMap();
    return map.getLocation(currentLocation);
  }

  bool locationHasShop() {
    Location? location = getLocation();
    if (location!.hasShop()) {
      return true;
    }
    return false;
  }

  List<Skill> getSkillTree() {
    List<Skill> skillTree = [];

    switch (getClass()) {
      case "Swordsman":
        skillTree.add(Skill("Thrust", 1, 3, 25, 1.5, 0.00, 25));
        skillTree.add(Skill("BackStab", 1, 3, 15, 1, 50.00, 25));
        break;
      case "Wizard":
        skillTree.add(Skill("Fireball", 1, 2, 30, 1.2, 20.00, 35));
        skillTree.add(Skill("Lighting bolt", 1, 2, 20, 1.3, 35.00, 35));
        break;
      case "Archer":
        skillTree.add(Skill("Double shot", 1, 2, baseAP, 1, 50.00, 10));
        skillTree.add(Skill("Precision shot", 1, 4, 10, 1, 100.00, 35));
        break;
      case "Cleric":
        skillTree.add(Skill("Smash", 1, 2, 50, 1.5, 20.00, 10));
        skillTree.add(HealingSkill("Heal", 1, 3, 0, 1.5, 15.00, 35, 150));
        break;
      default:
        break;
    }
    return skillTree;
  }

  void showSkill() {
    int count = 1;
    for (Skill skill in skillTree) {
      stdout.write("$count. [${skill.name}] ");
      switch (skill.skillLevel) {
        case 1:
          stdout.write("I");
          break;
        case 2:
          stdout.write("II");
          break;
        case 3:
          stdout.write("III");
          break;
        case 4:
          stdout.write("IV");
          break;
        case 5:
          stdout.write("V");
          break;
        default:
          break;
      }
      print(" MP cost: ${skill.MPcost}");
      count++;
    }
    print("$count. Back");
  }

  void skillCDController() {
    for (Skill skill in skillTree) {
      if (skill.isCoolDown()) {
        skill.currentSkillCD--;
      }
    }
  }

  void resetSkillCD() {
    for (Skill skill in skillTree) {
      skill.currentSkillCD = 0;
    }
  }

  int getTotalAP() {
    int totalDMG = 0;
    totalDMG += baseAP;
    if (mainHand != null) {
      totalDMG += mainHand!.attackPower;
    }
    if (offHand != null) {
      totalDMG += offHand!.attackPower;
    }
    return totalDMG;
  }

  bool sellItem() {
    if (inventory.isEmpty) {
      print("Inventory is Empty");
      return false;
    }
    showInventory(true);
    String input = stdin.readLineSync()!;
    if (selectSellItem(input)) {
      return true;
    }
    return false;
  }

  bool selectSellItem(String input) {
    if (inventory.isNotEmpty) {
      int count = int.parse(input);
      for (Item item in inventory) {
        count--;
        if (count != 0) {
          continue;
        }
        selling(item);
        return true;
      }
    }
    return false;
  }

  void selling(Item item) {
    if (item.amount == 1) {
      money += item.sellprice;
      inventory.remove(item);
      return;
    }
    while (true) {
      print("${item.name} x ${item.amount}\n   Price : ${item.sellprice}");
      print("Please enter amount : 1 -> ${item.amount}");
      String input = stdin.readLineSync()!;
      int num = int.parse(input);
      if (num >= item.amount) {
        num = item.amount;
      }
      money += item.sellprice * num;
      if (num == item.amount) {
        inventory.remove(item);
      } else {
        item.amount -= num;
      }
      return;
    }
  }

  bool upskill(int skillNum) {
    Skill skill = skillTree[skillNum - 1];
    if (skill.upgrade()) {
      return true;
    }
    return false;
  }

  void sortInventory() {
    inventory.sort((a, b) => a.name.compareTo(b.name));
  }

  List<Skill> getUnLearnSkill() {
    List<Skill> unlearnSkill = [];

    switch (getClass()) {
      case "Swordsman":
        unlearnSkill.add(Skill("Horizontal Slash", 1, 5, 50, 1.2, 20.00, 15));
        break;
      case "Wizard":
        unlearnSkill.add(Skill("Water ball", 1, 4, 50, 1.2, 35.00, 30));
        break;
      case "Archer":
        unlearnSkill.add(Skill("Weak point shot", 1, 5, 75, 1.1, 80.00, 70));
        break;
      case "Cleric":
        unlearnSkill.add(Skill("Holy smite", 1, 10, 100, 1.35, 10.00, 140));
        break;
      default:
        break;
    }
    return unlearnSkill;
  }

  void showUnLearnSkill() {
    int count = 1;
    for (Skill skill in unLearnSkill) {
      stdout.write("$count. [${skill.name}] ");
      switch (skill.skillLevel) {
        case 1:
          stdout.write("I");
          break;
        case 2:
          stdout.write("II");
          break;
        case 3:
          stdout.write("III");
          break;
        case 4:
          stdout.write("IV");
          break;
        case 5:
          stdout.write("V");
          break;
        default:
          break;
      }
      print(" MP cost: ${skill.MPcost}");
      count++;
    }
    print("$count. Back");
  }

  bool learnSkill(int skillNum) {
    try {
      skillTree.add(unLearnSkill.removeAt(skillNum - 1));
      return true;
    } catch (e) {
      return false;
    }
  }
}

class Skill {
  late String name;
  late int skillLevel;
  late int skillCD;
  int currentSkillCD = 0;
  late int skillBaseATK;
  late double attackAmplifier;
  late double tmpCritRate;
  late final int critRate;
  late int MPcost;

  Skill(this.name, this.skillLevel, this.skillCD, this.skillBaseATK,
      this.attackAmplifier, this.tmpCritRate, this.MPcost) {
    critRate = (tmpCritRate * 100).round();
  }

  int finalDamage(int playerBaseAP) {
    double tmp =
        ((playerBaseAP + (skillBaseATK * skillLevel)) * attackAmplifier) +
            Random().nextInt(10) -
            5;
    int intRand = Random().nextInt(10000) + 1;
    if (intRand <= critRate) {
      print("Critical!!!");
      return tmp.round() * 2;
    }
    return tmp.round();
  }

  bool isCoolDown() {
    if (currentSkillCD != 0) {
      return true;
    }
    return false;
  }

  void useSkill(Monster monster, Player player) {
    player.currentMP = player.currentMP! - MPcost;
    int finalDMG = finalDamage(player.getTotalAP());
    monster.currentHP -= finalDMG;
    stdout.write("You use [$name] ");
    switch (skillLevel) {
      case 1:
        stdout.write("I");
        break;
      case 2:
        stdout.write("II");
        break;
      case 3:
        stdout.write("III");
        break;
      case 4:
        stdout.write("IV");
        break;
      case 5:
        stdout.write("V");
        break;
      default:
        break;
    }
    print(" (-$finalDMG)");
    currentSkillCD = skillCD;
  }

  bool upgrade() {
    if (skillLevel < 5) {
      skillLevel++;
      double tmp = skillBaseATK * 1.12;
      skillBaseATK = tmp.round();
      attackAmplifier += 0.1;
      print("Skill $name is upgraded");
      return true;
    }
    print("Skill $name is maxed");
    return false;
  }
}

class HealingSkill extends Skill {
  late int healingPower;

  HealingSkill(String name, int skillLevel, int skillCD, int skillBaseATK,
      double attackAmplifier, double tmpCritRate, int MPcost, this.healingPower)
      : super(name, skillLevel, skillCD, skillBaseATK, attackAmplifier,
            tmpCritRate, MPcost);

  @override
  int finalDamage(int playerBaseAD) {
    return 0;
  }

  @override
  void useSkill(Monster monster, Player player) {
    double totalHeal =
        ((healingPower + player.getTotalAP()) * attackAmplifier) +
            Random().nextInt(10) -
            5;
    int intRand = Random().nextInt(10000) + 1;
    if (intRand <= critRate) {
      print("Critical!!!");
      totalHeal *= 2;
    }
    currentSkillCD = skillCD;
    player.currentMP = player.currentMP! - MPcost;
    player.currentHP = player.currentHP! + totalHeal.round();
    if (player.currentHP! > player.baseHP) {
      player.currentHP = player.baseHP;
    }
    stdout.write("You use [$name] ");
    switch (skillLevel) {
      case 1:
        stdout.write("I");
        break;
      case 2:
        stdout.write("II");
        break;
      case 3:
        stdout.write("III");
        break;
      case 4:
        stdout.write("IV");
        break;
      case 5:
        stdout.write("V");
        break;
      default:
        break;
    }
    print(" (+${totalHeal.round()}HP)");
  }
}

class Item {
  late String name;
  late bool stackable;
  late int amount;
  late int sellprice;
  late double dropRate;
  late final int dropRateNum;

  Item(this.name, this.stackable, this.amount, this.sellprice,
      [this.dropRate = 0]) {
    dropRateNum = (dropRate * 10000).round();
  }

  void showInfo() {}
}

class JunkItem extends Item {
  JunkItem(
      String name, bool stackable, int amount, int sellprice, double dropRate)
      : super(name, stackable, amount, sellprice, dropRate);
}

class Weapon extends Item {
  late String type;
  late int attackPower;
  bool isEquiped = false;

  Weapon(String name, this.type, this.attackPower, int sellPrice,
      [double dropRate = 0])
      : super(name, false, 1, sellPrice, dropRate);
}

abstract class Consumable extends Item {
  Consumable(String name, bool stackable, int amount, int sellPrice,
      [double dropRate = 0])
      : super(name, stackable, amount, sellPrice, dropRate);

  bool isUsable() {
    return false;
  }

  bool isCooldown() {
    return false;
  }

  void use(Player player) {}
}

class HealingPotion extends Consumable {
  int healingPower;

  HealingPotion(String name, this.healingPower, int amount, int sellPrice,
      [double dropRate = 0])
      : super(name, true, amount, sellPrice, dropRate);

  @override
  void use(Player player) {
    player.currentHP = player.currentHP! + healingPower;
    if (player.currentHP! > player.baseHP) {
      player.currentHP = player.baseHP;
    }
    print("Use Healing Potion (+${healingPower}HP)");
  }
}

class MagicPotion extends Consumable {
  int recoverPower;

  MagicPotion(String name, this.recoverPower, int amount, int sellPrice,
      [double dropRate = 0])
      : super(name, true, amount, sellPrice, dropRate);
}

class Ammunition extends Item {
  double damageScale;

  Ammunition(String name, this.damageScale, int amount, int sellPrice,
      [double dropRate = 0])
      : super(name, true, amount, sellPrice, dropRate);
}
